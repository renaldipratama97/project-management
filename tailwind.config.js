/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      fontFamily: {
        body: ["Roboto", "sans-serif"],
        logo1: ["Shadows Into Light", "cursive"],
        logo2: ["Reggae One", "cursive"],
      },
    },
  },
  plugins: [],
};
