import React from "react";
import { Link } from "react-router-dom";
import { StarFilled } from "@ant-design/icons";
import { Popover } from "antd";

const DropdownTentangKami = () => {
  return (
    <>
      <div className="w-[300px]">
        <ul className="w-full text-[#002B5B] text-[15px]">
          <Link
            to="/pages/sejarah"
            className="text-[#002B5B] hover:text-[#002B5B]"
          >
            <li className="flex items-center gap-3 h-10 rounded-md hover:bg-gray-200 pl-2 cursor-pointer">
              {" "}
              <StarFilled /> <span>Sejarah PT. Wahanakarsa Swandiri</span>
            </li>
          </Link>
          <Link
            to="/pages/visi-misi"
            className="text-[#002B5B] hover:text-[#002B5B]"
          >
            <li className="flex items-center gap-3 h-10 rounded-md hover:bg-gray-200 pl-2 cursor-pointer">
              {" "}
              <StarFilled /> <span>Visi Misi PT. Wahanakarsa Swandiri</span>
            </li>
          </Link>
          <Link
            to="/pages/struktur-organisasi"
            className="text-[#002B5B] hover:text-[#002B5B]"
          >
            <li className="flex items-center gap-3 h-10 rounded-md hover:bg-gray-200 pl-2 cursor-pointer">
              <StarFilled /> <span>Struktur Organisasi</span>
            </li>
          </Link>
        </ul>
      </div>
    </>
  );
};

export const Header = () => {
  return (
    <>
      <div className="w-100">
        <header className="w-full h-20 shadow-lg bg-[#002B5B] fixed top-0 z-[999]">
          <div className="w-11/12 h-full flex justify-between items-center mx-auto">
            <div className="flex flex-col justify-center items-center cursor-pointer">
              <h3 className="font-logo2 text-base text-gray-100 font-bold">
                PT. Wahanakarsa Swandiri
              </h3>
              <p className="font-logo2 text-xs text-gray-100 font-semibold">
                Construction Company
              </p>
            </div>
            <nav className="w-6/12">
              <ul className="w-full flex justify-end items-center text-gray-100 font-logo2 text-sm gap-10">
                <Link
                  to="/pages/home"
                  className="text-gray-100 hover:text-gray-300"
                >
                  <li className="cursor-pointer"> Home</li>
                </Link>
                <Popover content={DropdownTentangKami} trigger="click">
                  <li className="cursor-pointer"> Tentang Kami</li>
                </Popover>
                <Link
                  to="/pages/kontak"
                  className="text-gray-100 hover:text-gray-300"
                >
                  <li className="cursor-pointer"> Kontak</li>
                </Link>
                <Link
                  to="/auth/login"
                  className="text-gray-100 hover:text-gray-300"
                >
                  <li className="cursor-pointer">
                    <button className="bg-white text-[#002B5B] py-2 px-6 rounded-md hover:bg-slate-200">
                      Login
                    </button>
                  </li>
                </Link>
              </ul>
            </nav>
          </div>
        </header>
      </div>
    </>
  );
};
