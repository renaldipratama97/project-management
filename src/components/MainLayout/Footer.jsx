import React from "react";
import { useLocation } from "react-router-dom";

export const Footer = () => {
  const location = useLocation();
  return (
    <>
      {location.pathname !== "/auth/login" && (
        <footer className="w-full h-12 bg-[#002B5B] fixed bottom-0 flex justify-center items-center rounded-t-md">
          <div className="text-gray-100 font-logo2 text-sm flex justify-center items-center h-full">
            PT. Wahanakarsa Swandiri @2022
          </div>
        </footer>
      )}
    </>
  );
};
