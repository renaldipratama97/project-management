import React, { useEffect } from "react";
import { Popover, Avatar } from "antd";
import {
  UserOutlined,
  LogoutOutlined,
  SnippetsOutlined,
} from "@ant-design/icons";
import { Link, useLocation } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { logout } from "../../features/auth/authSlice";
import { useState } from "react";

const menuList = [
  {
    name: "Home",
    url: "/",
  },
  {
    name: "Project",
    url: "/projects",
  },
  {
    name: "Karyawan",
    url: "/karyawan",
  },
  {
    name: "User",
    url: "/user",
  },
  {
    name: "Report",
    url: "/",
  },
];

const PopoverContent = (no_karyawan) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const handleLogout = () => {
    dispatch(logout());
    navigate("/pages/home");
  };
  return (
    <>
      <div className="w-[150px]">
        <ul className="w-full text-[#002B5B] text-[15px]">
          <Link
            to={`user/${no_karyawan}`}
            className="text-[#002B5B] hover:text-[#002B5B]"
          >
            <li className="flex items-center gap-3 h-10 rounded-md hover:bg-gray-200 pl-2 cursor-pointer">
              {" "}
              <UserOutlined /> <span>Profile</span>
            </li>
          </Link>
          <li
            className="flex items-center gap-3 h-10 mt-2 rounded-md hover:bg-gray-200 pl-2 cursor-pointer"
            onClick={() => handleLogout()}
          >
            <LogoutOutlined /> <span>Logout</span>
          </li>
        </ul>
      </div>
    </>
  );
};

const PopoverContentReport = () => {
  return (
    <>
      <div className="w-[230px]">
        <ul className="w-full text-[#002B5B] text-[15px]">
          <Link
            to="/report/report_keuangan"
            className="text-[#002B5B] hover:text-[#002B5B]"
          >
            <li className="flex items-center gap-3 h-10 rounded-md hover:bg-gray-200 pl-2 cursor-pointer">
              {" "}
              <SnippetsOutlined /> <span>Report Uang Keluar</span>
            </li>
          </Link>
          <Link
            to="/report/report_keuangan_project"
            className="text-[#002B5B] hover:text-[#002B5B]"
          >
            <li className="flex items-center gap-3 h-10 rounded-md hover:bg-gray-200 pl-2 cursor-pointer">
              {" "}
              <SnippetsOutlined /> <span>Report Keuangan Project</span>
            </li>
          </Link>
          <Link
            to="/report/report_progress"
            className="text-[#002B5B] hover:text-[#002B5B]"
          >
            <li className="flex items-center gap-3 h-10 mt-2 rounded-md hover:bg-gray-200 pl-2 cursor-pointer">
              <SnippetsOutlined /> <span>Report Progress Project</span>
            </li>
          </Link>
        </ul>
      </div>
    </>
  );
};

export const Header = () => {
  const [dataMenu, setDataMenu] = useState([]);
  const location = useLocation();
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const { user } = useSelector((state) => state.auth);

  useEffect(() => {
    if (!user) {
      navigate("/pages/home");
    }

    if (user) {
      let tempMenu = [];
      if (user.level === "Administrator") {
        setDataMenu(menuList);
      } else if (user.level === "Manager") {
        tempMenu = menuList.filter(
          (item) => item.name !== "Karyawan" && item.name !== "User"
        );
        setDataMenu(tempMenu);
      } else if (user.level === "Team Leader") {
        tempMenu = menuList.filter(
          (item) =>
            item.name !== "Karyawan" &&
            item.name !== "User" &&
            item.name !== "Report"
        );
        setDataMenu(tempMenu);
      } else if (user.level === "Field Worker") {
        tempMenu = menuList.filter(
          (item) =>
            item.name !== "Karyawan" &&
            item.name !== "User" &&
            item.name !== "Report"
        );
        setDataMenu(tempMenu);
      }
    }
  }, [user, navigate, dispatch]);

  const cekData = (value) => {
    const x = value.split(" ");
    const array1 = x[0];
    const array2 = x[1];

    if (x.length > 1)
      return `${array1[0] === undefined ? "" : array1[0].toUpperCase()}${
        array2[0] === undefined ? "" : array2[0].toUpperCase()
      }`;
    else return `${array1[0].toUpperCase()}`;
  };

  return (
    <>
      {location.pathname !== "/auth/login" && (
        <header className="w-full h-20 shadow-lg bg-[#002B5B] rounded-b-md fixed top-0 z-[999]">
          <div className="w-11/12 h-full flex justify-between items-center mx-auto">
            <Link to="/">
              <div className="flex flex-col justify-center items-center cursor-pointer">
                <h3 className="font-logo2 text-base text-gray-100 font-bold">
                  PT. Wahanakarsa Swandiri
                </h3>
                <p className="font-logo2 text-xs text-gray-100 font-semibold">
                  Construction Company
                </p>
              </div>
            </Link>
            <nav className="w-6/12">
              <ul className="w-full flex justify-end items-center text-gray-100 font-logo2 text-sm gap-10">
                {dataMenu.map((item, index) => (
                  <li className="cursor-pointer" key={index}>
                    {item.name === "Report" ? (
                      <Popover content={PopoverContentReport} trigger="click">
                        {item.name}
                      </Popover>
                    ) : (
                      <Link
                        to={item.url}
                        className="text-gray-100 hover:text-gray-300"
                      >
                        {item.name}
                      </Link>
                    )}
                  </li>
                ))}

                <Popover
                  content={PopoverContent(user?.no_karyawan)}
                  trigger="click"
                >
                  <li className="cursor-pointer">
                    {/* <img
                      src="https://picsum.photos/200"
                      className="w-10 rounded-3xl border-white border-2"
                      alt="profile"
                    /> */}
                    <Avatar
                      style={{
                        backgroundColor: "#FFF",
                        verticalAlign: "middle",
                        color: "#002B5B",
                      }}
                      size="large"
                    >
                      {cekData(user ? user?.nama_karyawan : "X")}
                    </Avatar>
                  </li>
                </Popover>
              </ul>
            </nav>
          </div>
        </header>
      )}
    </>
  );
};
