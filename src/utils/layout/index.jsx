import React from "react";
import { Footer } from "../../components/MainLayout/Footer";
import { Header } from "../../components/MainLayout/Header";

export const index = (props) => {
  return (
    <>
      <div className="mt-20 w-full">
        <div>
          <div className="font-body overflow-hidden">
            <Header />
            <main className="w-10/12 mx-auto py-10">{props.children}</main>
            <Footer />
          </div>
        </div>
      </div>
    </>
  );
};
