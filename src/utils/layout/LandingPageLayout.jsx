import React from "react";
import { Header } from "../../components/LandingPageLayout/Header";

export const Layout = (props) => {
  return (
    <>
      <div className="w-full">
        <div>
          <div className="font-body overflow-hidden">
            <Header />
            <main className="w-10/12 mx-auto py-10">{props.children}</main>
          </div>
        </div>
      </div>
    </>
  );
};
