export const getTaskCompleted = (value) => {
  const completed = value.filter((val) => val.status === 4);

  return completed.length;
};

export const getTotalTask = (value) => {
  return value.length;
};

export const calculateBalanceOut = (value) => {
  let balance = 0;
  value.forEach((el) => {
    balance += el.balance_out;
  });

  return balance;
};
