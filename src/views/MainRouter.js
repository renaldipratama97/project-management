import React from "react";
import { index as Layout } from "../utils/layout/index";
import { Routes, Route } from "react-router-dom";
import Home from "./Home";
import User from "./User/index";
import Profile from "./User/Profile";
import Projects from "./Projects/index";
import TaskProject from "./Projects/TaskProject";
import Karyawan from "./Karyawan/index";
import ReportKeuangan from "./Report/ReportBalance";
import ReportProgress from "./Report/ReportProgress";
import ReportBalanceProject from "./Report/ReportBalanceProject";

const MainRouter = () => {
  return (
    <>
      <Layout>
        <Routes>
          <Route exact path="/" element={<Home />} />
          <Route path="/projects" element={<Projects />} />
          <Route path="/projects/:kode_project" element={<TaskProject />} />
          <Route path="/karyawan" element={<Karyawan />} />
          <Route path="/user" element={<User />} />
          <Route path="/user/:no_karyawan" element={<Profile />} />
          <Route path="/report/report_keuangan" element={<ReportKeuangan />} />
          <Route path="/report/report_progress" element={<ReportProgress />} />
          <Route
            path="/report/report_keuangan_project"
            element={<ReportBalanceProject />}
          />
        </Routes>
      </Layout>
    </>
  );
};

export default MainRouter;
