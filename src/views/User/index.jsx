/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-useless-escape */
import React, { useState, useEffect } from "react";
import {
  Table,
  Modal,
  Form,
  Input,
  Select,
  Skeleton,
  message,
  Space,
  Button,
  Tabs,
} from "antd";
import { EditOutlined, DeleteOutlined } from "@ant-design/icons";
import axios from "axios";
import { useSelector } from "react-redux";

const levelUser = [
  {
    label: "Administrator",
    value: "Administrator",
  },
  {
    label: "Manager",
    value: "Manager",
  },
  {
    label: "Team Leader",
    value: "Team Leader",
  },
  {
    label: "Field Worker",
    value: "Field Worker",
  },
];

const User = () => {
  const [form] = Form.useForm();
  const [formEdit] = Form.useForm();
  const [formChangePassword] = Form.useForm();
  const [openModal, setOpenModal] = useState(false);
  const [openModalEdit, setOpenModalEdit] = useState(false);
  const [employeeList, setEmployeeList] = useState([]);
  const [loading, setLoading] = useState(false);
  const [users, setUsers] = useState([]);
  const [kodeUser, setKodeUser] = useState("");
  const [currentUsername, setCurrentUsername] = useState("");

  const { user } = useSelector((state) => state.auth);

  const onHandleModal = () => {
    setOpenModal(!openModal);
    form.resetFields();
  };

  const onHandleModalEdit = () => {
    setOpenModalEdit(!openModalEdit);
    setKodeUser("");
    setCurrentUsername("");
    formEdit.resetFields();
    formChangePassword.resetFields();
  };

  const onFinish = async (values) => {
    // console.log("Success:", values);
    setLoading(true);
    try {
      const payload = {
        username: values.user_username,
        password: values.user_password,
        no_karyawan: values.user_employee,
        level: values.user_level,
      };

      const response = await axios.post(
        "http://localhost:1000/user/create",
        payload
      );

      if (response && response.data.statusCode === 201) {
        message.success("Sukses tambah karyawan");
      }

      if (response && response.data.statusCode === 304) {
        message.error(response.data.message);
      }
    } catch (error) {
      message.error(`failed : ${error}`);
    } finally {
      onHandleModal();
      getUsers();
      setLoading(false);
    }
  };

  const onFinishEdit = async (values) => {
    setLoading(true);
    try {
      const payload = {
        kode_user: kodeUser,
        currentUsername: currentUsername,
        username: values.user_username,
        level: values.user_level,
      };

      const response = await axios.post(
        "http://localhost:1000/user/update",
        payload
      );

      if (response && response.data.statusCode === 200) {
        message.success(response.data.message);
      }

      if (response && response.data.statusCode === 304) {
        message.error(response.data.message);
      }
    } catch (error) {
      message.error(`failed : ${error}`);
    } finally {
      onHandleModalEdit();
      getUsers();
      setLoading(false);
    }
  };

  const onFinishChangePassword = async (values) => {
    setLoading(true);
    try {
      const payload = {
        kode_user: kodeUser,
        password: values.user_password,
      };

      const response = await axios.post(
        "http://localhost:1000/user/update/change_password",
        payload
      );

      if (response && response.data.statusCode === 200) {
        message.success(response.data.message);
      }

      if (response && response.data.statusCode === 304) {
        message.error(response.data.message);
      }
    } catch (error) {
      message.error(`failed : ${error}`);
    } finally {
      onHandleModalEdit();
      getUsers();
      setLoading(false);
    }
  };

  const getEmployees = async () => {
    let tempEmployee = [];
    try {
      const response = await axios.get("http://localhost:1000/employees");

      if (response && response.data) {
        tempEmployee = response.data.data.map((item) => ({
          label: item.nama_karyawan,
          value: item.no_karyawan,
        }));
      }
    } catch (error) {
      console.log(error);
    } finally {
      setEmployeeList(tempEmployee);
    }
  };

  const getUsers = async () => {
    setLoading(true);
    try {
      const response = await axios.get("http://localhost:1000/user", {
        params: {
          kode_user: user.kode_user,
        },
      });

      if (response && response.data) {
        setUsers(response.data.data);
      }
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    getEmployees();
    getUsers();
  }, []);

  const confirmDelete = async (record) => {
    try {
      const payload = {
        no_karyawan: record,
      };
      const deleteEmployee = await axios.post(
        "http://localhost:1000/user/delete",
        payload
      );
      if (deleteEmployee) {
        message.success("Sukses hapus user");
      }
    } catch (error) {
      message.error(`failed : ${error}`);
    } finally {
      getUsers();
    }
  };

  const editUser = (value) => {
    setKodeUser(value.kode_user);
    setCurrentUsername(value.username);
    formEdit.setFieldsValue({
      user_username: value.username,
      user_level: value.level,
    });
    setOpenModalEdit(true);
  };

  const columns = [
    {
      title: "No Karyawan",
      dataIndex: "no_karyawan",
      key: "no_karyawan",
    },
    {
      title: "Nama Karyawan",
      dataIndex: "nama_karyawan",
      key: "nama_karyawan",
    },
    {
      title: "Username",
      dataIndex: "username",
      key: "username",
    },
    {
      title: "Level Akses",
      dataIndex: "level",
      key: "level",
    },
    {
      title: "Action",
      dataIndex: "no_karyawan",
      key: "no_karyawan",
      align: "center",
      render: (record, value) => {
        return (
          <Space size="middle">
            <Button type="primary" onClick={() => editUser(value)}>
              <EditOutlined />
            </Button>

            <Button type="danger" onClick={() => confirmDelete(record)}>
              <DeleteOutlined />
            </Button>
          </Space>
        );
      },
    },
  ];

  return (
    <>
      <div className="w-full flex justify-between items-center">
        <p className="text-lg font-bold">User</p>
        <button
          className="py-2 px-5 bg-[#002B5B] rounded-md text-gray-200 text-base font-medium"
          onClick={onHandleModal}
        >
          Tambah User
        </button>
      </div>

      <section className="mt-10 bg-white w-full shadow-xl p-3 rounded-md">
        {loading ? (
          <Skeleton />
        ) : (
          <Table columns={columns} dataSource={users} />
        )}
      </section>

      <Modal
        title="Form User"
        open={openModal}
        onOk={onHandleModal}
        onCancel={onHandleModal}
        footer={null}
      >
        <Form
          form={form}
          name="user_form"
          labelCol={{
            span: 24,
          }}
          wrapperCol={{
            span: 24,
          }}
          onFinish={onFinish}
          autoComplete="off"
          layout="vertical"
        >
          <Form.Item
            label="Nama Karyawan"
            name="user_employee"
            rules={[
              {
                required: true,
                message: "Please input employee name!",
              },
            ]}
          >
            <Select options={employeeList} size="large" allowClear />
          </Form.Item>
          <Form.Item
            label="Username"
            name="user_username"
            rules={[
              {
                required: true,
                message: "Please input username!",
              },
              {
                pattern: new RegExp("^\\S*$"),
                message: "Username tidak boleh menggunakan spasi!",
              },
            ]}
          >
            <Input size="large" allowClear />
          </Form.Item>
          <Form.Item
            label="Password"
            name="user_password"
            rules={[
              {
                required: true,
                message: "Please input password!",
              },
            ]}
          >
            <Input.Password size="large" allowClear />
          </Form.Item>
          <Form.Item
            label="Level User"
            name="user_level"
            rules={[
              {
                required: true,
                message: "Please input user level!",
              },
            ]}
          >
            <Select options={levelUser} size="large" allowClear />
          </Form.Item>
          <Form.Item>
            <div className="flex justify-between items-center w-full gap-3">
              <button
                onClick={onHandleModal}
                type="button"
                className="w-6/12 h-10 flex justify-center items-center bg-gray-500 text-white"
              >
                Cancel
              </button>
              <button
                type="submit"
                className="w-6/12 h-10 flex justify-center items-center bg-[#002B5B] text-white"
              >
                Simpan
              </button>
            </div>
          </Form.Item>
        </Form>
      </Modal>

      <Modal
        title="Form User"
        open={openModalEdit}
        onOk={onHandleModalEdit}
        onCancel={onHandleModalEdit}
        footer={null}
      >
        <Tabs defaultActiveKey="1">
          <Tabs.TabPane tab="Edit User" key="1">
            <Form
              form={formEdit}
              name="user_form_edit"
              labelCol={{
                span: 24,
              }}
              wrapperCol={{
                span: 24,
              }}
              onFinish={onFinishEdit}
              autoComplete="off"
              layout="vertical"
            >
              <Form.Item
                label="Username"
                name="user_username"
                rules={[
                  {
                    required: true,
                    message: "Please input username!",
                  },
                  {
                    pattern: new RegExp("^\\S*$"),
                    message: "Username tidak boleh menggunakan spasi!",
                  },
                ]}
              >
                <Input size="large" />
              </Form.Item>
              <Form.Item
                label="Level User"
                name="user_level"
                rules={[
                  {
                    required: true,
                    message: "Please input user level!",
                  },
                ]}
              >
                <Select options={levelUser} size="large" />
              </Form.Item>
              <Form.Item>
                <div className="flex justify-between items-center w-full gap-3">
                  <button
                    onClick={onHandleModalEdit}
                    type="button"
                    className="w-6/12 h-10 flex justify-center items-center bg-gray-500 text-white"
                  >
                    Cancel
                  </button>
                  <button
                    type="submit"
                    className="w-6/12 h-10 flex justify-center items-center bg-[#002B5B] text-white"
                  >
                    Update
                  </button>
                </div>
              </Form.Item>
            </Form>
          </Tabs.TabPane>
          <Tabs.TabPane tab="Change Password" key="2">
            <Form
              form={formChangePassword}
              name="user_form_edit"
              labelCol={{
                span: 24,
              }}
              wrapperCol={{
                span: 24,
              }}
              onFinish={onFinishChangePassword}
              autoComplete="off"
              layout="vertical"
            >
              <Form.Item
                label="Password"
                name="user_password"
                rules={[
                  {
                    required: true,
                    message: "Please input passwor!",
                  },
                  {
                    pattern: new RegExp("^\\S*$"),
                    message: "Password tidak boleh menggunakan spasi!",
                  },
                ]}
              >
                <Input.Password size="large" />
              </Form.Item>
              <Form.Item
                label="Konfirmasi Password"
                name="user_conf_password"
                dependencies={["user_password"]}
                rules={[
                  {
                    required: true,
                    message: "Please input konfirmasi password!",
                  },
                  ({ getFieldValue }) => ({
                    validator(_, value) {
                      if (!value || getFieldValue("user_password") === value) {
                        return Promise.resolve();
                      }
                      return Promise.reject(
                        new Error(
                          "Konfirmasi password belum sesuai dengan password!"
                        )
                      );
                    },
                  }),
                ]}
              >
                <Input.Password size="large" />
              </Form.Item>
              <Form.Item>
                <div className="flex justify-between items-center w-full gap-3">
                  <button
                    onClick={onHandleModalEdit}
                    type="button"
                    className="w-6/12 h-10 flex justify-center items-center bg-gray-500 text-white"
                  >
                    Cancel
                  </button>
                  <button
                    type="submit"
                    className="w-6/12 h-10 flex justify-center items-center bg-[#002B5B] text-white"
                  >
                    Change Password
                  </button>
                </div>
              </Form.Item>
            </Form>
          </Tabs.TabPane>
        </Tabs>
      </Modal>
    </>
  );
};

export default User;
