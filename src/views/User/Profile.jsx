import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { Modal, Form, Input, Select, Skeleton, message } from "antd";
import axios from "axios";

const gender = [
  {
    label: "Pria",
    value: "Pria",
  },
  {
    label: "Wanita",
    value: "Wanita",
  },
];

const Profile = () => {
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const [openModal, setOpenModal] = useState(false);
  const [employeeData, setEmployeeData] = useState({});
  const [userData, setUserData] = useState({});

  const { no_karyawan } = useParams();

  const onHandleModal = () => {
    setOpenModal(!openModal);
    form.resetFields();
  };

  const editProfile = () => {
    form.setFieldsValue({
      employee_number: employeeData.no_karyawan,
      employee_name: employeeData.nama_karyawan,
      employee_position: employeeData.jabatan,
      employee_gender: employeeData.jenkel,
      employee_address: employeeData.alamat,
      employee_phone: employeeData.no_handphone,
      user_username: userData.username,
    });

    setOpenModal(!openModal);
  };

  const getMe = async () => {
    setLoading(true);

    const response = await axios.get(
      `http://localhost:1000/user/profile/${no_karyawan}`
    );

    if (response && response.data.statusCode === 200) {
      setEmployeeData(response.data.data.employee);
      setUserData(response.data.data.user);
    }

    setLoading(false);
  };

  const onFinish = async (values) => {
    setLoading(true);
    try {
      const payload = {
        no_karyawan: values.employee_number,
        username: values.user_username,
        nama_karyawan: values.employee_name,
        jenkel: values.employee_gender,
        jabatan: values.employee_position,
        no_handphone: values.employee_phone,
        alamat: values.employee_address,
      };

      const response = await axios.post(
        "http://localhost:1000/user/profile/update",
        payload
      );

      if (response && response.data.statusCode === 200) {
        message.success(response.data.message);
      }
    } catch (error) {
      message.error(`failed : ${error}`);
    } finally {
      onHandleModal();
      setLoading(false);
      getMe();
    }
  };

  useEffect(() => {
    getMe();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [no_karyawan]);

  return (
    <>
      {loading ? (
        <Skeleton />
      ) : (
        <>
          <div className="w-6/12 flex justify-between items-center">
            <div className="text-lg font-bold">Profile</div>
            <button
              className="py-2 px-5 bg-[#002B5B] rounded-md text-gray-200 text-base font-medium"
              onClick={() => editProfile()}
            >
              Update Profile
            </button>
          </div>

          <div className="w-100 flex justify-between gap-3 mt-5">
            <div className="w-6/12 p-3 border rounded-md">
              <table className="text-base font-bold">
                <tr>
                  <td width={"150px"} height={"40px"}>
                    No Karyawan
                  </td>
                  <td width={"15px"}>:</td>
                  <td>{employeeData.no_karyawan}</td>
                </tr>
                <tr>
                  <td width={"150px"} height={"40px"}>
                    Nama
                  </td>
                  <td width={"15px"}>:</td>
                  <td>{employeeData.nama_karyawan}</td>
                </tr>
                <tr>
                  <td width={"150px"} height={"40px"}>
                    Username
                  </td>
                  <td width={"15px"}>:</td>
                  <td>{userData.username}</td>
                </tr>
                <tr>
                  <td width={"150px"} height={"40px"}>
                    Level User
                  </td>
                  <td width={"15px"}>:</td>
                  <td>{userData.level}</td>
                </tr>
                <tr>
                  <td width={"150px"} height={"40px"}>
                    Jabatan
                  </td>
                  <td>:</td>
                  <td>{employeeData.jabatan}</td>
                </tr>
                <tr>
                  <td width={"150px"} height={"40px"}>
                    Jenis Kelamin
                  </td>
                  <td>:</td>
                  <td>{employeeData.jenkel}</td>
                </tr>
                <tr>
                  <td width={"150px"} height={"40px"}>
                    No Handphone
                  </td>
                  <td>:</td>
                  <td>{employeeData.no_handphone}</td>
                </tr>
                <tr>
                  <td width={"150px"} height={"40px"}>
                    Alamat
                  </td>
                  <td>:</td>
                  <td>{employeeData.alamat}</td>
                </tr>
              </table>
            </div>
          </div>
        </>
      )}

      <Modal
        title="Form Update Profile"
        open={openModal}
        onOk={onHandleModal}
        onCancel={onHandleModal}
        footer={null}
      >
        {loading ? (
          <Skeleton />
        ) : (
          <Form
            form={form}
            name="user_profile_form"
            labelCol={{
              span: 24,
            }}
            wrapperCol={{
              span: 24,
            }}
            onFinish={onFinish}
            autoComplete="off"
            layout="vertical"
          >
            <Form.Item
              label="No Karyawan"
              name="employee_number"
              rules={[
                {
                  required: true,
                  message: "Please input employee number!",
                },
              ]}
            >
              <Input size="large" disabled />
            </Form.Item>
            <Form.Item
              label="Nama Karyawan"
              name="employee_name"
              rules={[
                {
                  required: true,
                  message: "Please input employee name!",
                },
              ]}
            >
              <Input size="large" />
            </Form.Item>
            <Form.Item
              label="Username"
              name="user_username"
              rules={[
                {
                  required: true,
                  message: "Please input username!",
                },
              ]}
            >
              <Input size="large" />
            </Form.Item>
            <Form.Item
              label="Jenis Kelamin"
              name="employee_gender"
              rules={[
                {
                  required: true,
                  message: "Please input employee gender!",
                },
              ]}
            >
              <Select options={gender} size="large" />
            </Form.Item>
            <Form.Item
              label="Jabatan"
              name="employee_position"
              rules={[
                {
                  required: true,
                  message: "Please input employee position!",
                },
              ]}
            >
              <Input size="large" />
            </Form.Item>
            <Form.Item
              label="No Handphone"
              name="employee_phone"
              rules={[
                {
                  required: true,
                  message: "Please input employee phone!",
                },
              ]}
            >
              <Input size="large" />
            </Form.Item>
            <Form.Item
              label="Alamat"
              name="employee_address"
              rules={[
                {
                  required: true,
                  message: "Please input employee address!",
                },
              ]}
            >
              <Input size="large" />
            </Form.Item>
            <Form.Item>
              <div className="flex justify-between items-center w-full gap-3">
                <button
                  onClick={onHandleModal}
                  type="button"
                  className="w-6/12 h-10 flex justify-center items-center bg-gray-500 text-white"
                >
                  Cancel
                </button>
                <button
                  type="submit"
                  className="w-6/12 h-10 flex justify-center items-center bg-[#002B5B] text-white"
                >
                  Update
                </button>
              </div>
            </Form.Item>
          </Form>
        )}
      </Modal>
    </>
  );
};

export default Profile;
