import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import {
  Modal,
  Form,
  Input,
  InputNumber,
  Select,
  Skeleton,
  Avatar,
  message,
  Tooltip,
  Button,
} from "antd";
import { stepProgress } from "../../constant/data.js";
import axios from "axios";
import moment from "moment";
const { TextArea } = Input;

const TaskProject = () => {
  const [comment, setComment] = useState("");
  const [statusUpdate, setStatusUpdate] = useState(false);
  const [dataComment, setDataComment] = useState([]);
  const [loading, setLoading] = useState(false);
  const [loadingDetail, setLoadingDetail] = useState(false);
  const [openModal, setOpenModal] = useState(false);
  const [openModalDetailTask, setOpenModalDetailTask] = useState(false);
  const [dataTaskModal, setDataTaskModal] = useState({});
  const [employeeAssignData, setEmployeeAssignData] = useState([]);
  const [teamLeaderData, setTeamLeaderData] = useState({});
  const [project, setProject] = useState({});
  const [taskOne, setTaskOne] = useState([]);
  const [taskTwo, setTaskTwo] = useState([]);
  const [taskThree, setTaskThree] = useState([]);
  const [taskFour, setTaskFour] = useState([]);
  const [employeeList, setEmployeeList] = useState([]);
  const [form] = Form.useForm();
  const { kode_project } = useParams();
  const navigate = useNavigate();

  const { user } = useSelector((state) => state.auth);

  const onHandleModalDetailTask = () => {
    setOpenModalDetailTask(!openModalDetailTask);
    getProjectByID(kode_project);
    setDataComment([]);
  };

  const openModalDetailTaskFunct = async (val) => {
    setLoading(true);
    try {
      const data = val.assign_to;
      const response = await axios.post(
        "http://localhost:1000/employee/get_employee",
        { data }
      );

      if (response && response.data) setEmployeeAssignData(response.data.data);
    } catch (error) {
      console.log(error);
    } finally {
      getComments(val.kode_task);
      setDataTaskModal(val);
      setOpenModalDetailTask(!openModalDetailTask);
      setLoading(false);
    }
  };

  const onHandleModal = () => {
    setOpenModal(!openModal);
    setStatusUpdate(false);
    form.resetFields();
  };

  const onFinish = async (values) => {
    console.log("values : ", values);
    setLoading(true);
    try {
      let response = null;
      const payloadUpdate = {
        kode_task: dataTaskModal.kode_task,
        nama_task: values.task_name,
        balance_out:
          values.balance_out === null || values.balance_out === undefined
            ? 0
            : values.balance_out,
        deskripsi_task:
          values.task_description === "" ||
          values.task_description === undefined
            ? null
            : values.task_description,
      };

      const payloadSave = {
        nama_task: values.task_name,
        balance_out:
          values.balance_out === null || values.balance_out === undefined
            ? 0
            : values.balance_out,
        deskripsi_task:
          values.task_description === "" ||
          values.task_description === undefined
            ? null
            : values.task_description,
        assign_to: JSON.stringify(values.task_assign),
        kode_project,
      };

      if (statusUpdate) {
        response = await axios.post(
          "http://localhost:1000/tasks/update",
          payloadUpdate
        );
      } else {
        response = await axios.post(
          "http://localhost:1000/tasks/create",
          payloadSave
        );
      }

      if (response && response?.data?.statusCode === 201) {
        message.success("Sukses tambah task");

        if (statusUpdate) {
          setDataTaskModal({
            ...dataTaskModal,
            nama_task: values.task_name,
            deskripsi_task:
              values.task_description === "" ||
              values.task_description === undefined
                ? null
                : values.task_description,
            balance_out:
              values.balance_out === null || values.balance_out === undefined
                ? 0
                : values.balance_out,
          });
        }
      }
    } catch (error) {
      message.error(`failed : ${error}`);
    } finally {
      onHandleModal();
      getTasks();
      setLoading(false);
    }
  };

  const getProjectByID = async (kode) => {
    setLoading(true);
    try {
      const response = await axios.get(`http://localhost:1000/project/${kode}`);

      if (response && response.data) {
        if (response.data.statusCode === 404) {
          return navigate("/projects");
        }

        setProject(response.data.data);
        await getTasks();
        await getEmployees(response?.data?.data?.assign);
        await getTeamLeader(response?.data?.data?.team_leader);
      }
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  };

  const getTasks = async () => {
    setLoading(true);
    let taskNotStarted = [];
    let taskProgress = [];
    let taskReview = [];
    let taskCompleted = [];
    try {
      const response = await axios.get(
        `http://localhost:1000/tasks/kode_project/${kode_project}`
      );

      if (response && response.data) {
        taskNotStarted = response.data.data.filter((val) => val.status === 1);
        taskProgress = response.data.data.filter((val) => val.status === 2);
        taskReview = response.data.data.filter((val) => val.status === 3);
        taskCompleted = response.data.data.filter((val) => val.status === 4);
      }
    } catch (error) {
      console.log(error);
    } finally {
      setTaskOne(taskNotStarted);
      setTaskTwo(taskProgress);
      setTaskThree(taskReview);
      setTaskFour(taskCompleted);
      setLoading(false);
    }
  };

  const getEmployees = async (data) => {
    setLoading(true);
    let tempEmployee = [];
    try {
      const response = await axios.post(
        "http://localhost:1000/employee/get_employee",
        { data }
      );

      if (response && response.data) {
        tempEmployee = response.data.data.map((item) => ({
          label: item.nama_karyawan,
          value: item.no_karyawan,
        }));
      }
    } catch (error) {
      console.log(error);
    } finally {
      setEmployeeList(tempEmployee);
      setLoading(false);
    }
  };

  const getTeamLeader = async (no_karyawan) => {
    try {
      const response = await axios.get(
        `http://localhost:1000/employees/${no_karyawan}`
      );

      if (response && response.data) {
        setTeamLeaderData(response.data.data);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const cekData = (value) => {
    const x = value.split(" ");
    const array1 = x[0];
    const array2 = x[1];

    if (x.length > 1)
      return `${array1[0] === undefined ? "" : array1[0].toUpperCase()}${
        array2[0] === undefined ? "" : array2[0].toUpperCase()
      }`;
    else return `${array1[0].toUpperCase()}`;
  };

  const changeTaskStatus = async (value, kode_task) => {
    setLoadingDetail(true);
    try {
      const payload = {
        status: value,
        kode_task,
      };
      const response = await axios.post(
        "http://localhost:1000/task/change_status",
        payload
      );

      if (response && response.data) {
        message.success(response.data.message);
      }
    } catch (error) {
      console.log(error);
    } finally {
      setLoadingDetail(false);
      onHandleModalDetailTask();
    }
  };

  const deleteTask = async (kode_task) => {
    setLoadingDetail(true);
    try {
      const response = await axios.post("http://localhost:1000/tasks/delete", {
        kode_task,
      });

      if (response && response.data) {
        message.success(response.data.message);
      }
    } catch (error) {
      console.log(error);
    } finally {
      setLoadingDetail(false);
      onHandleModalDetailTask();
    }
  };

  const submitComment = async () => {
    setLoadingDetail(true);
    try {
      const payload = {
        kode_task: dataTaskModal.kode_task,
        no_karyawan: user.no_karyawan,
        deskripsi: comment,
      };
      const response = await axios.post(
        "http://localhost:1000/comment/create",
        payload
      );

      if (response && response.data) {
        message.success(response.data.message);
      }
    } catch (error) {
      console.log(error);
    } finally {
      getComments(dataTaskModal.kode_task);
      setComment("");
      setLoadingDetail(false);
    }
  };

  const getComments = async (kode_task) => {
    setLoadingDetail(true);
    try {
      const response = await axios.get(
        `http://localhost:1000/comment/${kode_task}`
      );

      if (response && response.data) {
        setDataComment(response.data.data);
      }
    } catch (error) {
      console.log(error);
    } finally {
      setLoadingDetail(false);
    }
  };

  const editTask = () => {
    form.setFieldsValue({
      task_name: dataTaskModal.nama_task,
      balance_out: dataTaskModal.balance_out,
      task_description: dataTaskModal.deskripsi_task,
    });
    setOpenModal(true);
    setStatusUpdate(true);
  };

  useEffect(() => {
    getProjectByID(kode_project);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [kode_project]);

  return (
    <>
      <div className="w-full flex justify-between items-center">
        {!loading && (
          <>
            <div className="flex flex-col min-w-[100px]">
              <span className="text-lg font-bold">{project.nama_project}</span>
              <span className="text-sm text-gray-800 font-300">
                Lokasi : {project.lokasi}
              </span>
            </div>
            <div>
              <Avatar.Group>
                {employeeList.map((val) => (
                  <Tooltip title={val.label} placement="top">
                    <Avatar
                      key={val.value}
                      style={{
                        backgroundColor: "#002B5B",
                        verticalAlign: "middle",
                      }}
                      size="large"
                    >
                      {cekData(val.label)}
                    </Avatar>
                  </Tooltip>
                ))}
              </Avatar.Group>
            </div>
            {user.level === "Administrator" || user.level === "Team Leader" ? (
              <button
                onClick={onHandleModal}
                className="py-2 px-5 bg-[#002B5B] rounded-md text-gray-200 text-base font-medium"
              >
                Tambah Task
              </button>
            ) : (
              ""
            )}
          </>
        )}
      </div>

      {!loading ? (
        <section className="flex flex-wrap justify-between mt-10">
          <div className="w-[23%] border min-h-[208px] max-h-[400px] rounded-lg bg-slate-50">
            <div className="title-status h-10 bg-[#002B5B] rounded-tr-lg rounded-tl-lg border-[#002B5B] text-white flex justify-center items-center">
              Not Started
            </div>
            <div className="flex flex-col w-100 p-2 overflow-y-scroll max-h-[350px] no-scrollbar gap-1">
              {taskOne.map((val, index) => (
                <div
                  key={index}
                  onClick={() => openModalDetailTaskFunct(val)}
                  className="flex flex-col justify-between border rounded-md p-2 h-28 bg-white cursor-pointer"
                >
                  <div className="flex flex-col">
                    <span className="font-semibold text-[12px]">
                      {val.nama_task}
                    </span>
                    <span className="font-medium text-[12px] italic">
                      Rp {val.balance_out}
                    </span>
                  </div>
                  <div className="flex justify-between items-center">
                    <div className="text-[10px] text-gray-400 italic">
                      {moment(val.createdAt).format("lll")}
                    </div>
                    <div>
                      <Avatar
                        style={{
                          backgroundColor: "#002B5B",
                          verticalAlign: "middle",
                        }}
                        size="small"
                      >
                        -
                      </Avatar>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
          <div className="w-[23%] border min-h-[208px] max-h-[400px] rounded-lg bg-slate-50">
            <div className="title-status h-10 bg-[#002B5B] rounded-tr-lg rounded-tl-lg border-[#002B5B] text-white flex justify-center items-center">
              In Progress
            </div>

            <div className="flex flex-col w-100 p-2 overflow-y-scroll max-h-[350px] no-scrollbar gap-1">
              {taskTwo.map((val, index) => (
                <div
                  key={index}
                  onClick={() => openModalDetailTaskFunct(val)}
                  className="flex flex-col justify-between border rounded-md p-2 h-28 bg-white cursor-pointer"
                >
                  <div className="flex flex-col">
                    <span className="font-semibold text-[12px]">
                      {val.nama_task}
                    </span>
                    <span className="font-medium text-[12px] italic">
                      Rp {val.balance_out}
                    </span>
                  </div>
                  <div className="flex justify-between items-center">
                    <div className="text-[10px] text-gray-400 italic">
                      {moment(val.createdAt).format("lll")}
                    </div>
                    <div>
                      <Avatar
                        style={{
                          backgroundColor: "#002B5B",
                          verticalAlign: "middle",
                        }}
                        size="small"
                      >
                        -
                      </Avatar>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
          <div className="w-[23%] border min-h-[208px] max-h-[400px] rounded-lg bg-slate-50">
            <div className="title-status h-10 bg-[#002B5B] rounded-tr-lg rounded-tl-lg border-[#002B5B] text-white flex justify-center items-center">
              Cheking
            </div>

            <div className="flex flex-col w-100 p-2 overflow-y-scroll max-h-[350px] no-scrollbar gap-1">
              {taskThree.map((val, index) => (
                <div
                  key={index}
                  onClick={() => openModalDetailTaskFunct(val)}
                  className="flex flex-col justify-between border rounded-md p-2 h-28 bg-white cursor-pointer"
                >
                  <div className="flex flex-col">
                    <span className="font-semibold text-[12px]">
                      {val.nama_task}
                    </span>
                    <span className="font-medium text-[12px] italic">
                      Rp {val.balance_out}
                    </span>
                  </div>
                  <div className="flex justify-between items-center">
                    <div className="text-[10px] text-gray-400 italic">
                      {moment(val.createdAt).format("lll")}
                    </div>
                    <div>
                      <Avatar
                        style={{
                          backgroundColor: "#002B5B",
                          verticalAlign: "middle",
                        }}
                        size="small"
                      >
                        -
                      </Avatar>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
          <div className="w-[23%] border min-h-[208px] max-h-[400px] rounded-lg bg-slate-50">
            <div className="title-status h-10 bg-[#002B5B] rounded-tr-lg rounded-tl-lg border-[#002B5B] text-white flex justify-center items-center">
              Completed
            </div>

            <div className="flex flex-col w-100 p-2 overflow-y-scroll max-h-[350px] no-scrollbar gap-1">
              {taskFour.map((val, index) => (
                <div
                  key={index}
                  onClick={() => openModalDetailTaskFunct(val)}
                  className="flex flex-col justify-between border rounded-md p-2 h-28 bg-white cursor-pointer"
                >
                  <div className="flex flex-col">
                    <span className="font-semibold text-[12px]">
                      {val.nama_task}
                    </span>
                    <span className="font-medium text-[12px] italic">
                      Rp {val.balance_out}
                    </span>
                  </div>
                  <div className="flex justify-between items-center">
                    <div className="text-[10px] text-gray-400 italic">
                      {moment(val.createdAt).format("lll")}
                    </div>
                    <div>
                      <Avatar
                        style={{
                          backgroundColor: "#002B5B",
                          verticalAlign: "middle",
                        }}
                        size="small"
                      >
                        -
                      </Avatar>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </section>
      ) : (
        <>
          <Skeleton />
          <Skeleton />
          <Skeleton />
        </>
      )}

      <Modal
        title="Form Task Project"
        open={openModal}
        onOk={onHandleModal}
        onCancel={onHandleModal}
        footer={null}
      >
        <Form
          form={form}
          name="task_form"
          labelCol={{
            span: 24,
          }}
          wrapperCol={{
            span: 24,
          }}
          onFinish={onFinish}
          autoComplete="off"
          layout="vertical"
        >
          <Form.Item
            label="Nama Task"
            name="task_name"
            rules={[
              {
                required: true,
                message: "Please input task name!",
              },
            ]}
          >
            <Input size="large" maxLength={30} />
          </Form.Item>
          {!statusUpdate && (
            <Form.Item
              label="Assign to"
              name="task_assign"
              rules={[
                {
                  required: true,
                  message: "Please select employee!",
                },
              ]}
            >
              <Select mode="tags" options={employeeList} size="large" />
            </Form.Item>
          )}
          <Form.Item label="Uang Keluar" name="balance_out">
            <InputNumber
              formatter={(value) =>
                `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
              }
              style={{ width: "100%" }}
              size="large"
            />
          </Form.Item>
          <Form.Item label="Deskripsi Task" name="task_description">
            <TextArea rows={4} />
          </Form.Item>
          <Form.Item>
            <div className="flex justify-between items-center w-full gap-3">
              <button
                onClick={onHandleModal}
                type="button"
                className="w-6/12 h-10 flex justify-center items-center bg-gray-500 text-white"
              >
                Cancel
              </button>
              <button
                type="submit"
                className="w-6/12 h-10 flex justify-center items-center bg-[#002B5B] text-white"
              >
                {statusUpdate ? "Update" : "Simpan"}
              </button>
            </div>
          </Form.Item>
        </Form>
      </Modal>

      <Modal
        open={openModalDetailTask}
        onOk={onHandleModalDetailTask}
        onCancel={onHandleModalDetailTask}
        footer={null}
        closable={false}
        width={800}
      >
        <div className="flex w-100 gap-2">
          {loadingDetail ? (
            <Skeleton />
          ) : (
            <>
              <div className="w-[70%] h-full border rounded-lg p-2">
                <h3 className="text-base font-bold text-black">
                  {dataTaskModal?.nama_task}
                </h3>
                <div className="flex flex-col">
                  {dataTaskModal?.deskripsi_task && (
                    <>
                      <h4 className="text-sm font-bold text-black">
                        Deskripsi Task :
                      </h4>
                      <div className="text-[13px] font-semibold text-black">
                        {dataTaskModal?.deskripsi_task}
                      </div>
                    </>
                  )}
                  <div className="flex flex-col mt-5 gap-1">
                    <h4 className="text-sm font-bold text-black">Comment :</h4>
                    <div className="flex flex-col gap-2 w-full">
                      {dataComment.map((val, idx) => (
                        <div
                          key={idx}
                          className="flex w-full justify-between border p-2 rounded-md"
                        >
                          <div className="w-[10%]">
                            <Tooltip title={val.nama_karyawan} placement="top">
                              <Avatar
                                style={{
                                  backgroundColor: "#002B5B",
                                  verticalAlign: "middle",
                                }}
                                size="default"
                              >
                                {cekData(
                                  val?.nama_karyawan ? val?.nama_karyawan : "x"
                                )}
                              </Avatar>
                            </Tooltip>
                          </div>
                          <div className="flex flex-col w-[90%]">
                            <div className="flex items-center gap-3">
                              <span className="text-sm font-semibold text-black">
                                {val.nama_karyawan}
                              </span>
                              <span className="text-xs font-semibold italic text-gray-500">
                                {moment(val.createdAt).format("lll")}
                              </span>
                            </div>
                            <div className="text-[13px] font-semibold text-gray-700">
                              {val.deskripsi}
                            </div>
                          </div>
                        </div>
                      ))}
                    </div>
                  </div>

                  <div className="w-100 h-10 mt-2 flex justify-between items-center">
                    <div className="w-[75%]">
                      <Input
                        placeholder="Input your comments"
                        onChange={(e) => setComment(e.target.value)}
                        onPressEnter={() => submitComment()}
                      />
                    </div>
                    <div className="w-[20%] border">
                      <button
                        className="bg-[#002B5B] w-[100%] h-[32px] rounded-sm text-white"
                        onClick={() => submitComment()}
                      >
                        Submit
                      </button>
                    </div>
                  </div>
                </div>
              </div>
              <div className="w-[30%] border rounded-lg max-h-[400px] p-1 overflow-y-scroll no-scrollbar">
                {user.level === "Administrator" ||
                user.level === "Team Leader" ? (
                  <div className="flex items-center w-[100%] gap-1">
                    <div className="w-[50%]">
                      <Button
                        type="primary"
                        style={{ borderRadius: "6px", width: "100%" }}
                        onClick={() => editTask()}
                      >
                        Edit Task
                      </Button>
                    </div>
                    <div className="w-[50%]">
                      <Button
                        type="danger"
                        style={{ borderRadius: "6px", width: "100%" }}
                        onClick={() => deleteTask(dataTaskModal.kode_task)}
                      >
                        Delete Task
                      </Button>
                    </div>
                  </div>
                ) : (
                  ""
                )}
                <div
                  className={
                    user.level === "Administrator" ||
                    user.level === "Team Leader"
                      ? "flex flex-col mt-4"
                      : "flex flex-col"
                  }
                >
                  <h4 className="text-sm font-bold text-black">
                    Status Task :{" "}
                  </h4>
                  <Select
                    value={dataTaskModal.status}
                    style={{ width: "100%" }}
                    options={stepProgress}
                    onChange={(value) =>
                      changeTaskStatus(value, dataTaskModal.kode_task)
                    }
                  />
                </div>
                <div className="flex flex-col mt-4">
                  <h4 className="text-sm font-bold text-black">
                    Team Leader :
                  </h4>
                  <div className="flex items-center w-100">
                    <div className="w-[15%]">
                      <Tooltip
                        title={teamLeaderData.nama_karyawan}
                        placement="top"
                      >
                        <Avatar
                          style={{
                            backgroundColor: "#002B5B",
                            verticalAlign: "middle",
                          }}
                          size="small"
                        >
                          {cekData(
                            teamLeaderData?.nama_karyawan
                              ? teamLeaderData?.nama_karyawan
                              : "x"
                          )}
                        </Avatar>
                      </Tooltip>
                    </div>
                    <span className="w-[85%] text-sm font-normal text-gray-700">
                      {teamLeaderData.nama_karyawan}
                    </span>
                  </div>
                </div>
                <div className="flex flex-col mt-4">
                  <h4 className="text-sm font-bold text-black">Assign to :</h4>
                  {employeeAssignData.map((el, index) => (
                    <div key={index} className="flex items-center mt-1 w-100">
                      <div className="w-[15%]">
                        <Tooltip title={el.nama_karyawan} placement="top">
                          <Avatar
                            style={{
                              backgroundColor: "#002B5B",
                              verticalAlign: "middle",
                            }}
                            size="small"
                          >
                            {cekData(el.nama_karyawan)}
                          </Avatar>
                        </Tooltip>
                      </div>
                      <span className="w-[85%] text-sm font-normal text-gray-700">
                        {el.nama_karyawan}
                      </span>
                    </div>
                  ))}
                </div>
              </div>
            </>
          )}
        </div>
      </Modal>
    </>
  );
};

export default TaskProject;
