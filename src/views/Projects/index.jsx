import React, { useState, useEffect } from "react";
import {
  Modal,
  Form,
  Input,
  InputNumber,
  Select,
  message,
  Skeleton,
  Button,
  Progress,
  Tooltip,
} from "antd";
import { DeleteOutlined, EditOutlined, EyeOutlined } from "@ant-design/icons";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import {
  getTaskCompleted,
  calculateBalanceOut,
  getTotalTask,
} from "../../utils/helper";

const Projects = () => {
  const [form] = Form.useForm();
  const [openModal, setOpenModal] = useState(false);
  const [projects, setProjects] = useState([]);
  const [leaderList, setLeaderList] = useState([]);
  const [fieldWorkerList, setFieldWorkerList] = useState([]);
  const [loading, setLoading] = useState(false);
  const [projectCode, setProjectCode] = useState("");
  const [statusUpdate, setStatusUpdate] = useState(false);

  const navigate = useNavigate();

  const { user } = useSelector((state) => state.auth);

  const onHandleModal = () => {
    setOpenModal(!openModal);
    setProjectCode("");
    form.resetFields();
  };

  const getProjects = async () => {
    setLoading(true);
    try {
      const response = await axios.get(
        "http://localhost:1000/project/for-report"
      );

      if (response && response.data) {
        setProjects(response.data.data);
      }
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  };

  const confirmDelete = async (record) => {
    try {
      const payload = {
        kode_project: record,
      };
      const deleteEmployee = await axios.post(
        "http://localhost:1000/project/delete",
        payload
      );
      if (deleteEmployee) {
        message.success("Sukses hapus project");
      }
    } catch (error) {
      message.error(`failed : ${error}`);
    } finally {
      getProjects();
    }
  };

  const editProject = async (value) => {
    setLoading(true);
    try {
      const response = await axios.get(
        `http://localhost:1000/project/${value.kode_project}`
      );
      if (response && response.data.statusCode === 200) {
        setProjectCode(response.data.data.kode_project);
        form.setFieldsValue({
          project_name: response.data.data.nama_project,
          project_balance: response.data.data.balance,
          team_leader: response.data.data.team_leader,
          employees: JSON.parse(response.data.data.assign),
          project_location: response.data.data.lokasi,
        });
      }
    } catch (error) {
      console.log(error);
    } finally {
      setStatusUpdate(true);
      setOpenModal(true);
      setLoading(false);
    }
  };

  const getEmployees = async () => {
    let tempEmployeeLeader = [];
    let tempEmployeeFW = [];
    try {
      const response = await axios.get("http://localhost:1000/employeexuser");

      if (response && response.data) {
        tempEmployeeLeader = response.data.data
          // eslint-disable-next-line array-callback-return
          .map((item) => {
            if (item.level === "Team Leader") {
              return { label: item.nama_karyawan, value: item.no_karyawan };
            }
          })
          .filter((val) => val);

        tempEmployeeFW = response.data.data
          // eslint-disable-next-line array-callback-return
          .map((item) => {
            if (item.level === "Field Worker") {
              return { label: item.nama_karyawan, value: item.no_karyawan };
            }
          })
          .filter((val) => val);
      }
    } catch (error) {
      console.log(error);
    } finally {
      setLeaderList(tempEmployeeLeader);
      setFieldWorkerList(tempEmployeeFW);
    }
  };

  const onFinish = async (values) => {
    setLoading(true);
    try {
      let response = null;
      const payloadUpdate = {
        kode_project: projectCode,
        nama_project: values.project_name,
        balance: values.project_balance,
        team_leader: values.team_leader,
        lokasi: values.project_location,
        assign: JSON.stringify(values.employees),
      };

      const payloadSave = {
        nama_project: values.project_name,
        balance: values.project_balance,
        team_leader: values.team_leader,
        lokasi: values.project_location,
        assign: JSON.stringify(values.employees),
      };
      if (statusUpdate) {
        response = await axios.post(
          "http://localhost:1000/project/update",
          payloadUpdate
        );
      } else {
        response = await axios.post(
          "http://localhost:1000/project/create",
          payloadSave
        );
      }
      if (response && response.data.statusCode === 201) {
        message.success("Sukses tambah project");
      }
    } catch (error) {
      message.error(`failed : ${error}`);
    } finally {
      onHandleModal();
      getProjects();
      setLoading(false);
      setStatusUpdate(false);
    }
  };

  const toProjectTask = (value) => {
    navigate(`/projects/${value.kode_project}`);
  };

  useEffect(() => {
    getEmployees();
    getProjects();
  }, []);

  return (
    <>
      <div className="w-full flex justify-between items-center">
        <p className="text-lg font-bold">Projects</p>
        {user.level === "Administrator" && (
          <button
            className="py-2 px-5 bg-[#002B5B] rounded-md text-gray-200 text-base font-medium"
            onClick={onHandleModal}
          >
            Tambah Project
          </button>
        )}
      </div>

      {loading ? (
        <Skeleton />
      ) : (
        <section className="flex flex-wrap justify-between gap-5 mt-10">
          {projects.map((val) => (
            <div
              key={val.kode_project}
              className="bg-gradient-to-l from-gray-200 shadow-sm rounded-md w-[49%] p-3 cursor-pointer h-52 border border-gray-200"
            >
              <div className="flex w-full items-center">
                <Tooltip title={val.nama_project} placement="top">
                  <div className="text-lg font-bold w-[70%] text-ellipsis whitespace-nowrap overflow-hidden">
                    {val.nama_project}
                  </div>
                </Tooltip>

                <div className="w-[30%] flex justify-end gap-2">
                  <Button type="primary" onClick={() => editProject(val)}>
                    <EditOutlined />
                  </Button>
                  <Button type="info" onClick={() => toProjectTask(val)}>
                    <EyeOutlined />
                  </Button>
                  <Button
                    type="danger"
                    onClick={() => confirmDelete(val.kode_project)}
                  >
                    <DeleteOutlined />
                  </Button>
                </div>
              </div>
              <div className="w-full flex flex-col mt-14">
                <div className="flex items-center text-[12px] font-bold">
                  Uang Keluar{"  "}
                  <span className="italic text-gray-400 text-[12px] font-normal">
                    {" "}
                    [{calculateBalanceOut(val.task).toLocaleString("en-US")}/
                    {val.balance.toLocaleString("en-US")}]
                  </span>
                </div>
                <Progress
                  percent={Math.floor(
                    (calculateBalanceOut(val.task) / val.balance) * 100
                  )}
                  status="active"
                />
              </div>
              <div className="w-full flex flex-col">
                <div className="flex items-center text-[12px] font-bold">
                  Progress Task{" "}
                  <span className="italic text-gray-400 text-[12px] font-normal">
                    {" "}
                    [{getTaskCompleted(val.task)}/{getTotalTask(val.task)}]
                  </span>
                </div>
                <Progress
                  percent={
                    (getTaskCompleted(val.task) / getTotalTask(val.task)) * 100
                  }
                  status="active"
                />
              </div>
            </div>
          ))}
        </section>
      )}

      <Modal
        title="Form Project"
        open={openModal}
        onOk={onHandleModal}
        onCancel={onHandleModal}
        footer={null}
        forceRender
      >
        <Form
          form={form}
          name="project_form"
          labelCol={{
            span: 24,
          }}
          wrapperCol={{
            span: 24,
          }}
          onFinish={onFinish}
          autoComplete="off"
          layout="vertical"
        >
          <Form.Item
            label="Nama Project"
            name="project_name"
            rules={[
              {
                required: true,
                message: "Please input project name!",
              },
            ]}
          >
            <Input size="large" />
          </Form.Item>
          <Form.Item
            label="Dana Project"
            name="project_balance"
            rules={[
              {
                required: true,
                message: "Please input project balance!",
              },
            ]}
          >
            <InputNumber
              size="large"
              formatter={(value) =>
                `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
              }
              style={{ width: "100%" }}
            />
          </Form.Item>
          <Form.Item
            label="Team Leader"
            name="team_leader"
            rules={[
              {
                required: true,
                message: "Please select team leader!",
              },
            ]}
          >
            <Select options={leaderList} size="large" />
          </Form.Item>
          <Form.Item
            label="Karyawan"
            name="employees"
            rules={[
              {
                required: true,
                message: "Please select employees!",
              },
            ]}
          >
            <Select mode="tags" options={fieldWorkerList} size="large" />
          </Form.Item>
          <Form.Item
            label="Lokasi Project"
            name="project_location"
            rules={[
              {
                required: true,
                message: "Please input project location!",
              },
            ]}
          >
            <Input size="large" />
          </Form.Item>
          <Form.Item>
            <div className="flex justify-between items-center w-full gap-3">
              <button
                onClick={onHandleModal}
                type="button"
                className="w-6/12 h-10 flex justify-center items-center bg-gray-500 text-white"
              >
                Cancel
              </button>
              <button
                type="submit"
                className="w-6/12 h-10 flex justify-center items-center bg-[#002B5B] text-white"
              >
                {statusUpdate ? "Update" : "Simpan"}
              </button>
            </div>
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};

export default Projects;
