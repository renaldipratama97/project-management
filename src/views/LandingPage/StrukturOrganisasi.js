import React from "react";
const dataTable = [
  {
    id: 1,
    nama: "Basnil Badrul",
    jabatan: "Project Director",
  },
  {
    id: 2,
    nama: "Edison Sembiring",
    jabatan: "Project Manager",
  },
  {
    id: 3,
    nama: "Gusnaidi",
    jabatan: "Area Supt",
  },
  {
    id: 4,
    nama: "Marlin Said",
    jabatan: "Const Equip Supt",
  },
  {
    id: 5,
    nama: "Erisman",
    jabatan: "QA/QE Supt",
  },
  {
    id: 6,
    nama: "Zulherman",
    jabatan: "Site Piping Engineering",
  },
  {
    id: 7,
    nama: "Faisal Syah",
    jabatan: "HES Supt",
  },
  {
    id: 8,
    nama: "Gustina S Fitra",
    jabatan: "Project Control Supt",
  },
  {
    id: 9,
    nama: "Irwan",
    jabatan: "Site Mech Engineer",
  },
  {
    id: 10,
    nama: "Sukiyanto",
    jabatan: "HES Engineer/Safety Officer",
  },
  {
    id: 11,
    nama: "Yudhi Fadli",
    jabatan: "Planner",
  },
  {
    id: 12,
    nama: "M. Eka Yudha",
    jabatan: "Site E/I Engineer",
  },
  {
    id: 13,
    nama: "Muhammad Firdaus",
    jabatan: "Quantity Surveyor",
  },
  {
    id: 14,
    nama: "Zakirman",
    jabatan: "Cost Engineer",
  },
];

const StrukturOrganisasi = () => {
  return (
    <>
      <div className="mt-[120px]">
        <h4 className="text-4xl font-bold">Struktur Organisasi</h4>
        <div className="w-100 border border-dashed"></div>

        <h6 className="text-[30px] font-bold text-gray-500 mt-10">
          Struktur Organisasi PT. Wahanakarsa Swandiri
        </h6>
        <div className="w-6/12 mt-3">
          <img
            src={require("../../assets/diagram-jabatan.jpg")}
            alt="struktur-jabatan"
          />
        </div>
      </div>
      <div className="w-6/12 mt-10">
        <h6 className="text-[30px] font-bold text-gray-500 mt-10">
          Struktur Jabatan PT. Wahanakarsa Swandiri
        </h6>
        <div className="flex flex-col">
          <div className="overflow-x-auto">
            <div className="p-1.5 w-full inline-block align-middle">
              <div className="overflow-hidden border rounded-lg">
                <table className="min-w-full divide-y divide-gray-200">
                  <thead className="bg-gray-50">
                    <tr>
                      <th
                        scope="col"
                        className="px-3 py-3 text-xs font-bold text-left text-gray-500 uppercase "
                      >
                        No
                      </th>
                      <th
                        scope="col"
                        className="px-6 py-3 text-xs font-bold text-left text-gray-500 uppercase "
                      >
                        Nama
                      </th>
                      <th
                        scope="col"
                        className="px-6 py-3 text-xs font-bold text-left text-gray-500 uppercase "
                      >
                        Jabatan
                      </th>
                    </tr>
                  </thead>
                  <tbody className="divide-y divide-gray-200">
                    {dataTable.map((item, index) => (
                      <tr key={index}>
                        <td className="px-3 py-4 text-sm font-medium text-gray-800 whitespace-nowrap">
                          {item.id}
                        </td>
                        <td className="px-6 py-4 text-sm text-gray-800 whitespace-nowrap">
                          {item.nama}
                        </td>
                        <td className="px-6 py-4 text-sm text-gray-800 whitespace-nowrap">
                          {item.jabatan}
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default StrukturOrganisasi;
