import React from "react";

const VisiMisi = () => {
  return (
    <>
      <div className="mt-[120px]">
        <h4 className="text-4xl font-bold">Visi & Misi</h4>
        <div className="w-100 border border-dashed"></div>

        <h6 className="text-[30px] font-bold text-gray-500 mt-10">
          Visi PT. Wahanakarsa Swandiri
        </h6>
        <p className="w-6/12 text-base">
          Visi PT. Wahanakarsa Swandiri adalah “Menjadi Perusahaan Konstruksi
          Kelas Dunia”.
        </p>

        <h6 className="text-[30px] font-bold text-gray-500 mt-10">
          Misi PT. Wahanakarsa Swandiri
        </h6>
        <ol className="w-6/12 text-base">
          <li className="flex">
            <span className="mr-3">1.</span> Menjadi perusahaan pilihan terutama
            di industri minyak, gas dan energi serta proyek infrastruktur
            lainnya di Indonesia.
          </li>
          <li className="flex">
            <span className="mr-3">2.</span> Memberikan nilai tambah kepada
            Pelanggan, karyawan, dan pemegang saham yang selaras dengan strategi
            bisnis.
          </li>
          <li className="flex">
            <span className="mr-3">3.</span> Mengembangkan organisasi dan
            kompetensi di setiap lingkup pelayanan.
          </li>
          <li className="flex">
            <span className="mr-3">4.</span> Memberikan Layanan Kompetitif dalam
            Kualitas, Keamanan dan Kinerja.
          </li>
        </ol>
      </div>
    </>
  );
};

export default VisiMisi;
