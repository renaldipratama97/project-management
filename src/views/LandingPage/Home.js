import React from "react";

const Home = () => {
  return (
    <>
      <div className="mt-[120px]">
        <h4 className="text-4xl font-bold">Home</h4>
        <div className="w-100 border border-dashed"></div>

        <div className="w-12/12 flex gap-3 justify-between items-center mt-10">
          <div className="w-4/12 h-[500px] overflow-hidden">
            <img
              src={require("../../assets/wahana2.jpeg")}
              className="w-full h-[99%] cursor-pointer rounded-tl-xl rounded-bl-xl object-cover transition-all ease-out duration-300 hover:scale-105"
              draggable="false"
              alt="image1"
            />
          </div>
          <div className="w-8/12 h-[500px] flex justify-between flex-wrap">
            <div className="h-[49%] w-[49%] overflow-hidden">
              <img
                src={require("../../assets/wahana1.jpeg")}
                className="w-full h-full cursor-pointer object-cover transition-all ease-out duration-300 hover:scale-105"
                draggable="false"
                alt="image1"
              />
            </div>
            <div className="h-[49%] w-[50%] overflow-hidden">
              <img
                src={require("../../assets/wahana3.jpeg")}
                className="w-full h-full cursor-pointer rounded-tr-xl object-cover transition-all ease-out duration-300 hover:scale-105"
                draggable="false"
                alt="image1"
              />
            </div>
            <div className="h-[49%] w-[49%] overflow-hidden">
              <img
                src={require("../../assets/wahana4.jpeg")}
                className="w-full h-full cursor-pointer object-cover transition-all ease-out duration-300 hover:scale-105"
                draggable="false"
                alt="image1"
              />
            </div>
            <div className="h-[49%] w-[50%] overflow-hidden">
              <img
                src={require("../../assets/wahana5.jpeg")}
                className="w-full h-full cursor-pointer rounded-br-xl object-cover transition-all ease-out duration-300 hover:scale-105"
                draggable="false"
                alt="image1"
              />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Home;
