import React from "react";
import {
  PhoneFilled,
  MailFilled,
  PushpinFilled,
  HourglassFilled,
} from "@ant-design/icons";

const Kontak = () => {
  return (
    <>
      <div className="mt-[120px]">
        <h4 className="text-4xl font-bold">Kontak</h4>
        <div className="w-100 border border-dashed"></div>
        <div className="mt-10 flex justify-between items-center w-12/12 gap-3">
          <div className="flex flex-col justify-center items-center w-3/12 border py-4 cursor-pointer group hover:bg-[#002B5B]">
            <div className="text-[50px] group-hover:text-white">
              <PhoneFilled />
            </div>
            <div className="text-2xl font-extrabold mt-6 group-hover:text-white">
              Phone
            </div>
            <div className="text-md font-extralight text-gray-400 group-hover:text-white">
              (0761) 6700527
            </div>
          </div>
          <div className="flex flex-col justify-center items-center w-3/12 border py-4 cursor-pointer group hover:bg-[#002B5B]">
            <div className="text-[50px] group-hover:text-white">
              <PushpinFilled />
            </div>
            <div className="text-2xl font-extrabold mt-6 group-hover:text-white">
              Address
            </div>
            <div className="text-md font-extralight text-gray-400 group-hover:text-white">
              Jl. Arifin Ahmad No.10, Pekanbaru
            </div>
          </div>
          <div className="flex flex-col justify-center items-center w-3/12 border py-4 cursor-pointer group hover:bg-[#002B5B]">
            <div className="text-[50px] group-hover:text-white">
              <HourglassFilled />
            </div>
            <div className="text-2xl font-extrabold mt-6 group-hover:text-white">
              Open Time
            </div>
            <div className="text-md font-extralight text-gray-400 group-hover:text-white">
              08:00 am to 18:00 pm
            </div>
          </div>
          <div className="flex flex-col justify-center items-center w-3/12 border py-4 cursor-pointer group hover:bg-[#002B5B]">
            <div className="text-[50px] group-hover:text-white">
              <MailFilled />
            </div>
            <div className="text-2xl font-extrabold mt-6 group-hover:text-white">
              Mail
            </div>
            <div className="text-md font-extralight text-gray-400 group-hover:text-white">
              recruitment@wahanakarsa.co.id
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Kontak;
