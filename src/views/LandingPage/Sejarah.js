import React from "react";

const Sejarah = () => {
  return (
    <>
      <div className="mt-[120px]">
        <h4 className="text-4xl font-bold">Sejarah</h4>
        <div className="w-100 border border-dashed"></div>

        <h6 className="text-[30px] font-bold text-gray-500 mt-10">
          Sejarah PT. Wahanakarsa Swandiri
        </h6>
        <p className="w-6/12 text-base">
          PT. Wahanakarsa Swandiri pertama kali didirikan di Jakarta pada bulan
          November 1988. Kantor pusat PT. Wahanakarsa Swandiri berada di Jl.
          Wahid Hasyim No.62A Jakarta Pusat, sedangkan kantor cabang yang berada
          di pekanbaru berada di Jl. Arifin Ahmad No.10, Sidomulyo Tim., Kec.
          Marpoyan Damai, Kota Pekanbaru. PT. Wahanakarsa Swandiri bergerak di
          bidang kontruksi dan jasa. President Director saat ini yaitu Bapak
          Dasrul.
        </p>
        <p className="w-6/12 text-base">
          PT. Wahanakarsa Swandiri memiliki beberapa cabang di beberapa kota di
          Indonesia dan mempunyai Kerjasama di beberapa perusahaan perusahaan
          lain, seperti:
        </p>
        <ol className="w-6/12 text-base">
          <li>1. PT. Chevron Pacific Indonesia</li>
          <li>2. PT. Riau Andalas Pulp & Paper</li>
          <li>3. PT. BOB</li>
          <li>4. PT. Pertamina</li>
          <li>5. PT. Semen Padang</li>
        </ol>
      </div>
    </>
  );
};

export default Sejarah;
