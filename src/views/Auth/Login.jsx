import React, { useEffect } from "react";
import { Form, Input, message } from "antd";
import { useNavigate, Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { login, reset } from "../../features/auth/authSlice";
import { RollbackOutlined, LoginOutlined } from "@ant-design/icons";

const Login = () => {
  const [form] = Form.useForm();

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const { user, isLoading, isError, isSuccess, messages } = useSelector(
    (state) => state.auth
  );

  useEffect(() => {
    if (isError) {
      message.error(messages);
    }

    if (isSuccess || user) {
      navigate("/");
    }

    dispatch(reset());
  }, [user, isError, isSuccess, messages, navigate, dispatch]);

  const onFinish = async (values) => {
    try {
      const payload = {
        username: values.username,
        password: values.password,
      };

      dispatch(login(payload));
    } catch (error) {
      message.error(`failed : ${error}`);
    }
  };
  return (
    <>
      <div className="w-full h-screen flex justify-center items-center pb-[150px]">
        <div className="w-4/12">
          <div className="w-100 text-center font-bold text-[35px] text-[#002B5B] font-logo2">
            PT. Wahanakarsa Swandiri
          </div>
          <div className="mt-5">
            <Form
              form={form}
              name="login_form"
              labelCol={{
                span: 24,
              }}
              wrapperCol={{
                span: 24,
              }}
              onFinish={onFinish}
              autoComplete="off"
              layout="vertical"
            >
              <Form.Item
                className="form-input-login"
                label="Username"
                name="username"
                rules={[
                  {
                    required: true,
                    message: "Please input username!",
                  },
                ]}
              >
                <Input size="large" />
              </Form.Item>
              <Form.Item
                className="form-input-login"
                label="Password"
                name="password"
                rules={[
                  {
                    required: true,
                    message: "Please input password!",
                  },
                ]}
              >
                <Input.Password size="large" />
              </Form.Item>
              <Form.Item>
                <div className="flex justify-between items-center w-full">
                  <Link
                    to="/pages/home"
                    className="w-3/12 h-10 flex justify-center gap-2 items-center hover:bg-black hover:text-white bg-[#c5c5c5] text-[#002B5B]"
                  >
                    <div className="w-12/12 h-10 flex justify-center gap-2 items-center hover:bg-black">
                      <RollbackOutlined />
                      Kembali
                    </div>
                  </Link>
                  <button
                    disabled={isLoading}
                    type="submit"
                    className="w-3/12 h-10 flex justify-center gap-2 items-center bg-[#002B5B] text-white"
                  >
                    Sign in
                    <LoginOutlined />
                  </button>
                </div>
              </Form.Item>
            </Form>
          </div>
        </div>
      </div>
    </>
  );
};

export default Login;
