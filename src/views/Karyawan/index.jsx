import {
  Table,
  Modal,
  Form,
  Input,
  Select,
  message,
  Skeleton,
  Space,
  Button,
} from "antd";
import { useSelector } from "react-redux";
import { EditOutlined, DeleteOutlined } from "@ant-design/icons";
import React, { useState, useEffect } from "react";
import axios from "axios";

const gender = [
  {
    label: "Pria",
    value: "Pria",
  },
  {
    label: "Wanita",
    value: "Wanita",
  },
];
const Karyawan = () => {
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);
  const [openModal, setOpenModal] = useState(false);
  const [employeeList, setEmployeeList] = useState([]);
  const [status, setStatus] = useState(false);

  const { user } = useSelector((state) => state.auth);

  const onHandleModal = () => {
    setOpenModal(!openModal);
    setStatus(false);
    form.resetFields();
  };

  const onFinish = async (values) => {
    setLoading(true);
    try {
      let response = null;
      const payload = {
        no_karyawan: values.id,
        nama_karyawan: values.employee_name,
        jenkel: values.employee_gender,
        jabatan: values.employee_position,
        no_handphone: values.employee_phone,
        alamat: values.employee_address,
      };

      if (status) {
        response = await axios.post(
          "http://localhost:1000/employee/update",
          payload
        );
      } else {
        response = await axios.post(
          "http://localhost:1000/employee/create",
          payload
        );
      }

      if (response && response.data.statusCode === 201) {
        message.success(response.data.message);
      }
    } catch (error) {
      message.error(`failed : ${error}`);
    } finally {
      onHandleModal();
      getEmployees();
      setLoading(false);
    }
  };

  const getEmployees = async () => {
    setLoading(true);
    try {
      const response = await axios.get("http://localhost:1000/employees");

      if (response && response.data) {
        setEmployeeList(response.data.data);
      }
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    getEmployees();
  }, []);

  const confirmDelete = async (record) => {
    try {
      const payload = {
        no_karyawan: record,
      };
      const deleteEmployee = await axios.post(
        "http://localhost:1000/employee/delete",
        payload
      );

      if (deleteEmployee) {
        message.success("Sukses hapus karyawan");
      }
    } catch (error) {
      message.error(`failed : ${error}`);
    } finally {
      getEmployees();
    }
  };

  const editEmployee = async (value) => {
    form.setFieldsValue({
      id: value.no_karyawan,
      employee_name: value.nama_karyawan,
      employee_position: value.jabatan,
      employee_gender: value.jenkel,
      employee_address: value.alamat,
      employee_phone: value.no_handphone,
    });
    setStatus(true);
    setOpenModal(true);
  };

  const columns = [
    {
      title: "No Karyawan",
      dataIndex: "no_karyawan",
      key: "no_karyawan",
    },
    {
      title: "Nama Karyawan",
      dataIndex: "nama_karyawan",
      key: "nama_karyawan",
    },
    {
      title: "Jenis Kelamin",
      dataIndex: "jenkel",
      key: "jenkel",
    },
    {
      title: "Jabatan",
      dataIndex: "jabatan",
      key: "jabatan",
    },
    {
      title: "No Handphone",
      dataIndex: "no_handphone",
      key: "no_handphone",
    },
    {
      title: "Alamat",
      dataIndex: "alamat",
      key: "alamat",
    },
    {
      title: "Action",
      dataIndex: "no_karyawan",
      key: "no_karyawan",
      align: "center",
      render: (record, value) => {
        return (
          <Space size="middle">
            <Button type="primary" onClick={() => editEmployee(value)}>
              <EditOutlined />
            </Button>

            <Button type="danger" onClick={() => confirmDelete(record)}>
              <DeleteOutlined />
            </Button>
          </Space>
        );
      },
    },
  ];

  return (
    <>
      <div className="w-full flex justify-between items-center">
        <p className="text-lg font-bold">Karyawan</p>
        {user.level === "Administrator" && (
          <button
            className="py-2 px-5 bg-[#002B5B] rounded-md text-gray-200 text-base font-medium"
            onClick={onHandleModal}
          >
            Tambah Karyawan
          </button>
        )}
      </div>

      <section className="mt-10 bg-white w-full shadow-xl p-3 rounded-md border">
        {loading ? (
          <Skeleton />
        ) : (
          <Table columns={columns} dataSource={employeeList} />
        )}
      </section>

      <Modal
        title="Form Karyawan"
        open={openModal}
        onOk={onHandleModal}
        onCancel={onHandleModal}
        footer={null}
      >
        {loading ? (
          <Skeleton />
        ) : (
          <Form
            form={form}
            name="employee_form"
            labelCol={{
              span: 24,
            }}
            wrapperCol={{
              span: 24,
            }}
            onFinish={onFinish}
            autoComplete="off"
            layout="vertical"
          >
            <Form.Item
              label="No Karyawan"
              name="id"
              disabled={status}
              rules={[
                {
                  required: true,
                  message: "Please input employee id!",
                },
              ]}
            >
              <Input size="large" disabled={status} />
            </Form.Item>
            <Form.Item
              label="Nama Karyawan"
              name="employee_name"
              rules={[
                {
                  required: true,
                  message: "Please input employee name!",
                },
              ]}
            >
              <Input size="large" />
            </Form.Item>
            <Form.Item
              label="Jenis Kelamin"
              name="employee_gender"
              rules={[
                {
                  required: true,
                  message: "Please input employee gender!",
                },
              ]}
            >
              <Select options={gender} size="large" />
            </Form.Item>
            <Form.Item
              label="Jabatan"
              name="employee_position"
              rules={[
                {
                  required: true,
                  message: "Please input employee position!",
                },
              ]}
            >
              <Input size="large" />
            </Form.Item>
            <Form.Item
              label="No Handphone"
              name="employee_phone"
              rules={[
                {
                  required: true,
                  message: "Please input employee phone!",
                },
              ]}
            >
              <Input size="large" />
            </Form.Item>
            <Form.Item
              label="Alamat"
              name="employee_address"
              rules={[
                {
                  required: true,
                  message: "Please input employee address!",
                },
              ]}
            >
              <Input size="large" />
            </Form.Item>
            <Form.Item>
              <div className="flex justify-between items-center w-full gap-3">
                <button
                  onClick={onHandleModal}
                  type="button"
                  className="w-6/12 h-10 flex justify-center items-center bg-gray-500 text-white"
                >
                  Cancel
                </button>
                <button
                  type="submit"
                  className="w-6/12 h-10 flex justify-center items-center bg-[#002B5B] text-white"
                >
                  {status ? "Update" : "Simpan"}
                </button>
              </div>
            </Form.Item>
          </Form>
        )}
      </Modal>
    </>
  );
};

export default Karyawan;
