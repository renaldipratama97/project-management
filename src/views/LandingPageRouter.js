import React from "react";
import { Layout } from "../utils/layout/LandingPageLayout";
import { Routes, Route } from "react-router-dom";
import IndexLandingPage from "./LandingPage/Home.js";
import Kontak from "./LandingPage/Kontak.js";
import Sejarah from "./LandingPage/Sejarah.js";
import VisiMisi from "./LandingPage/VisiMisi.js";
import StrukturOrganisasi from "./LandingPage/StrukturOrganisasi.js";

const LandingPageRouter = () => {
  return (
    <>
      <Layout>
        <Routes>
          <Route exact path="/home" element={<IndexLandingPage />} />
          <Route exact path="/kontak" element={<Kontak />} />
          <Route exact path="/sejarah" element={<Sejarah />} />
          <Route exact path="/visi-misi" element={<VisiMisi />} />
          <Route
            exact
            path="/struktur-organisasi"
            element={<StrukturOrganisasi />}
          />
        </Routes>
      </Layout>
    </>
  );
};

export default LandingPageRouter;
