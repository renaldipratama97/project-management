import React, { useState, useEffect } from "react";
import { Skeleton, Table } from "antd";
import axios from "axios";
import { saveAs } from "file-saver";

const ReportKeuangan = () => {
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState([false]);

  const getData = async () => {
    setLoading(true);
    try {
      const response = await axios.get(
        "http://localhost:1000/tasks/bybalance/get"
      );

      if (response && response.data) {
        setData(response.data.data);
      }
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  };

  const downloadPDF = () => {
    axios
      .post("http://localhost:1000/api/create-pdf-balance")
      .then(() =>
        axios.get("http://localhost:1000/api/fetch-pdf", {
          responseType: "blob",
        })
      )
      .then((res) => {
        const pdfBlob = new Blob([res.data], { type: "application/pdf" });

        saveAs(pdfBlob, "newPdf.pdf");
      });
  };

  useEffect(() => {
    getData();
  }, []);

  const columns = [
    {
      title: "Nama Task",
      dataIndex: "nama_task",
      key: "nama_task",
    },
    {
      title: "Tanggal",
      dataIndex: "createdAt",
      key: "createdAt",
    },
    {
      title: "Uang Keluar",
      dataIndex: "balance_out",
      key: "balance_out",
      render: (record) => {
        return <div>Rp {record ? record.toLocaleString("en-US") : 0}</div>;
      },
    },
  ];
  return (
    <>
      <div className="w-full flex justify-between items-center">
        <p className="text-lg font-bold">Report Uang Keluar</p>
        <button
          className="py-2 px-5 bg-[#002B5B] rounded-md text-gray-200 text-base font-medium"
          onClick={() => downloadPDF()}
        >
          Download PDF
        </button>
      </div>

      <section className="mt-10 bg-white w-full shadow-xl p-3 rounded-md border">
        {loading ? (
          <Skeleton />
        ) : (
          <Table columns={columns} dataSource={data} pagination={false} />
        )}
      </section>
    </>
  );
};

export default ReportKeuangan;
