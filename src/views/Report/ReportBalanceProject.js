import React, { useState, useEffect } from "react";
import { Skeleton, Table } from "antd";
import axios from "axios";
import { saveAs } from "file-saver";

const ReportBalanceProject = () => {
  const [loading, setLoading] = useState(false);
  const [dataProjects, setDataProjects] = useState([]);

  const getProjects = async () => {
    setLoading(true);
    try {
      const response = await axios.get(
        "http://localhost:1000/project/for-report"
      );

      if (response && response.data) {
        setDataProjects(response.data.data);
      }
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  };

  const downloadPDF = () => {
    axios
      .post("http://localhost:1000/api/create-pdf-balance-project")
      .then(() =>
        axios.get("http://localhost:1000/api/fetch-pdf", {
          responseType: "blob",
        })
      )
      .then((res) => {
        const pdfBlob = new Blob([res.data], { type: "application/pdf" });

        saveAs(pdfBlob, "balanceProject.pdf");
      });
  };

  useEffect(() => {
    getProjects();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const columns = [
    {
      title: "Nama Project",
      dataIndex: "nama_project",
      key: "nama_project",
    },
    {
      title: "Dana Project",
      dataIndex: "balance",
      key: "balance",
      render: (record) => {
        return <div>Rp {record ? record.toLocaleString("en-US") : 0}</div>;
      },
    },
    {
      title: "Total Penggunaan Dana",
      dataIndex: "task",
      key: "task",
      render: (record) => {
        let total = 0;
        record.forEach((val) => {
          total += val.balance_out;
        });
        return <div>Rp {total.toLocaleString("en-US")}</div>;
      },
    },
    {
      title: "Total Penggunaan Dana (%)",
      dataIndex: "task",
      key: "task",
      render: (record, value) => {
        let total = 0;
        record.forEach((val) => {
          total += val.balance_out;
        });

        const percentTotal = (total / value.balance) * 100;
        return <div>{percentTotal.toLocaleString("en-US")} %</div>;
      },
    },
    {
      title: "Sisa Dana Project",
      dataIndex: "task",
      key: "task",
      render: (record, value) => {
        let total = 0;
        record.forEach((val) => {
          total += val.balance_out;
        });
        const sisaDana = value.balance - total;
        return <div>Rp {sisaDana.toLocaleString("en-US")}</div>;
      },
    },
  ];
  return (
    <>
      <div className="w-full flex justify-between items-center">
        <p className="text-lg font-bold">Report Progress</p>
        <button
          className="py-2 px-5 bg-[#002B5B] rounded-md text-gray-200 text-base font-medium"
          onClick={() => downloadPDF()}
        >
          Download PDF
        </button>
      </div>

      <section className="mt-10 bg-white w-full shadow-xl p-3 rounded-md border">
        {loading ? (
          <Skeleton />
        ) : (
          <Table
            columns={columns}
            dataSource={dataProjects}
            pagination={false}
          />
        )}
      </section>
    </>
  );
};

export default ReportBalanceProject;
