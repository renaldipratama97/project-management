import React, { useEffect, useState } from "react";
import axios from "axios";
import {
  BriefcaseIcon,
  UserGroupIcon,
  UsersIcon,
  TicketIcon,
} from "@heroicons/react/24/solid";
import { Skeleton } from "antd";

const Home = () => {
  const [data, setData] = useState({});
  const [loading, setLoading] = useState(false);

  const getCounts = async () => {
    setLoading(true);
    const response = await axios.get("http://localhost:1000/api/get-counts");

    if (response && response.data.statusCode === 200) {
      setData(response.data.data);
    }
    setLoading(false);
  };

  useEffect(() => {
    getCounts();
  }, []);

  return (
    <>
      <div className="w-full flex justify-between items-center">
        <p className="text-lg font-bold">Home</p>
      </div>

      {loading ? (
        <Skeleton />
      ) : (
        <>
          <div className="w-12/12 flex items-center gap-5 mt-[50px]">
            <div className="w-[25%] h-[150px] flex p-3 rounded-md rounded-tr-[30px] rounded-bl-[30px] bg-[#002B5B] cursor-pointer">
              <div className="w-[50%] p-3">
                <UserGroupIcon className="text-white" />
              </div>
              <div className="flex flex-col items-center justify-center w-[50%] h-[100%]">
                <div className="flex justify-center items-center text-2xl font-bold text-white">
                  {data.employee}
                </div>
                <div className="flex justify-center items-center text-white text-3xl">
                  Karyawan
                </div>
              </div>
            </div>

            <div className="w-[25%] h-[150px] flex p-3 rounded-md rounded-tr-[30px] rounded-bl-[30px] bg-[#002B5B] cursor-pointer">
              <div className="w-[50%] p-3">
                <UsersIcon className="text-white" />
              </div>
              <div className="flex flex-col items-center justify-center w-[50%] h-[100%]">
                <div className="flex justify-center items-center text-2xl font-bold text-white">
                  {data.user}
                </div>
                <div className="flex justify-center items-center text-white text-3xl">
                  User
                </div>
              </div>
            </div>

            <div className="w-[25%] h-[150px] flex p-3 rounded-md rounded-tr-[30px] rounded-bl-[30px] bg-[#002B5B] cursor-pointer">
              <div className="w-[50%] p-3">
                <BriefcaseIcon className="text-white" />
              </div>
              <div className="flex flex-col items-center justify-center w-[50%] h-[100%]">
                <div className="flex justify-center items-center text-2xl font-bold text-white">
                  {data.project}
                </div>
                <div className="flex justify-center items-center text-white text-3xl">
                  Project
                </div>
              </div>
            </div>

            <div className="w-[25%] h-[150px] flex p-3 rounded-md rounded-tr-[30px] rounded-bl-[30px] bg-[#002B5B] cursor-pointer">
              <div className="w-[50%] p-3">
                <TicketIcon className="text-white" />
              </div>
              <div className="flex flex-col items-center justify-center w-[50%] h-[100%]">
                <div className="flex justify-center items-center text-2xl font-bold text-white">
                  {data.task}
                </div>
                <div className="flex justify-center items-center text-white text-3xl">
                  Task
                </div>
              </div>
            </div>
          </div>
        </>
      )}
    </>
  );
};

export default Home;
