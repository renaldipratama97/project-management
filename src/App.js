import React, { useEffect } from "react";
// import Cookies from "js-cookie";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Login from "./views/Auth/Login.jsx";
import MainRouter from "./views/MainRouter.js";
import LandingPageLayout from "./views/LandingPageRouter.js";

function App() {
  useEffect(() => {
    document.title = "PT. Wahanakarsa Swandiri";
  }, []);

  return (
    <>
      <Router>
        <Routes>
          <Route path="/auth/login" element={<Login />} />
          <Route path="/pages/*" element={<LandingPageLayout />} />
          <Route exact path="/*" element={<MainRouter />} />
        </Routes>
      </Router>
    </>
  );
}

export default App;
