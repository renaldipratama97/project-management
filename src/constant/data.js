export const stepProgress = [
  {
    label: "Not Started",
    value: 1,
  },
  {
    label: "In Progress",
    value: 2,
  },
  {
    label: "Cheking",
    value: 3,
  },
  {
    label: "Completed",
    value: 4,
  },
];
